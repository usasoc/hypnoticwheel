package com.example.lynn.hypnoticwheel;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class DrawingView extends View {
	private int radius;
	private Paint paint;

	public DrawingView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		radius = 10;
		
		paint = new Paint();
	}
	
	public void updateRadii() {
		radius += 10;

        if (radius >= 200)
            radius = 10;
		
		invalidate();
	}
	
	public void onDraw(Canvas canvas) {
		
		for (int counter=radius;counter>=0;counter-=10) {		
		   paint.setColor(0xFFFF0000);
		
		   canvas.drawCircle(400,400,counter+5,paint);
		
		   paint.setColor(0xFFFFFFFF);
		
		   canvas.drawCircle(400,400,counter,paint);
		}
		
		
		/*
		paint.setColor(0xFFFF0000);
		
		canvas.drawCircle(400, 400, 400, paint);
		
		paint.setColor(0xFFFFFFFF);
		
		canvas.drawCircle(400,400,300,paint);
		*/

	}

}
