package com.example.lynn.hypnoticwheel;

import static com.example.lynn.hypnoticwheel.MainActivity.*;

/**
 * Created by lynn on 3/14/2015.
 */
public class MyThread implements Runnable {
    private boolean keepGoing;

    public MyThread() {
        keepGoing = true;

        thread = new Thread(this);

        thread.start();
    }

    private void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    public void stop() {
        keepGoing = false;
    }

    public void run() {
        while (keepGoing) {
            drawingView.post(new Runnable() {

                public void run() {
                    drawingView.updateRadii();
                }

            });

            pause(0.5);
        }

    }

}
